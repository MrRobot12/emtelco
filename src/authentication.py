'''
    custom_auth by Juan David González Bedoya
    CustomBackend modified by por Andrés Felipe Blandón Palacio, noviembre 2018
'''

import requests
from datetime import timedelta, datetime
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib.auth.backends import ModelBackend
from django.utils import timezone
from django.conf import settings
from rest_framework_jwt import utils

from app.models import LOGIN_APPLICATION, User
from app.serializer import UserSerializer


class CustomBackend(ModelBackend):
    '''
    Autenticación personalizada, se deben definir dos metodos por lo menos
    authenticate()
    get_user()

    Documentación oficial
    https://docs.djangoproject.com/en/1.11/topics/auth/customizing/#specifying-authentication-backends

    Leer los siguientes títulos para entender el proceso
        - Specifying authentication backends
        - Writing an authentication backend
    '''

    def authenticate(self, request, username=None, password=None):
        '''
        Autenticar el usuario por aplicación o directorio activo.
        Se ejecuta cuando se llama el authenticate() del paquete django.contrib.auth.autehnticate
        y debe de estar configurado en el settings (lista llamada AUTHENTICATION_BACKENDS)

        request: objecto de la petición actual
        username: username que se envió por la petición
        password: password que se envió por la petición
        '''
        try:
            # Consultar el usuario en la aplicación solo por username
            user_exist_django = User.objects.get(username=username) 
            

            if user_exist_django.login_type == LOGIN_APPLICATION:
                # Chequear que el password coincida con el de aplicación
                if not user_exist_django.check_password(password):
                    user_exist_django = None
            else:
                print(user_exist_django.is_superuser)
                # Si el password no es el de aplicación, buscarlo en el directorio activo
                user_exist_ldap = requests.post(
                    settings.ACTIVE_DIRECTORY_URL,
                    {
                        'user': username,
                        'password': password
                    }
                )
                content = user_exist_ldap.json()

                # Si la respuestea es incorrecta se retorna el mensaje de validación
                if not (user_exist_ldap.status_code == requests.codes['ok']):
                    user_exist_django = None
                    if user_exist_ldap.status_code == requests.codes['internal_server_error']:
                        raise ValidationError(content['general'])

                user_exist_ldap.close()
                

            if not user_exist_django.is_superuser and not user_exist_django.has_perm('appemtelco.add_piloto') and not user_exist_django.piloto:
                raise ValidationError({'non_field_errors': 'El usuario no tiene un piloto asignado.'})
    
        except User.DoesNotExist:
            # None cuando NO existe el usuario en aplicación
            user_exist_django = None
        except ObjectDoesNotExist:
            # None cuando no encuentra información en user_data
            user_exist_django = None
        
        # Retornar la instancia encontrada o en su defecto un None
        return user_exist_django


def jwt_response_payload_handler(token, user=None, request=None):
    '''
    Personalizar los datos que se devolveran después de que un usuario se autentique
    por medio del jwt

    Se ejecuta después de que jwt autentique el usuario. Se configura en el settings,
    en el diccionario JWT_AUTH con clave JWT_RESPONSE_PAYLOAD_HANDLER

    Leer el siguiente link para entender el proceso
    https://getblimp.github.io/django-rest-framework-jwt/#additional-settings

    token:      token generado cuando el usuario se autentico
    user:       instancia del usuario autenticado
    request:    objeto de la petición actual
    '''

    # Info del usuario a retornar
    user_info = UserSerializer(user)

    # Crear lista de permisos y consultar todos los grupos asociados al usuario
    permissions = []
    groups = user.groups.all()
    user_permissions = user.user_permissions.all()

    # Recorrer los grupos
    for group in groups:
        # Recorrer todos los permisos de cada grupo y asignarlos
        for permission in group.permissions.all():
            permissions.append(permission.codename)

    # Recorrer los permisos directos al usuario
    for permission in user_permissions:
        if permission not in permissions:
            permissions.append(permission.codename)

    # Retornar el token y la info que deseas (Esta debe ser serializable)
    return {
        'token': token,
        'user': user_info.data,
        'permissions': permissions
    }


def jwt_payload_handler(user):
    payload = utils.jwt_payload_handler(user)

    return payload
