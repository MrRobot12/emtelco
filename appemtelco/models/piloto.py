from django.db import models

COLOR_RED = 'red'
COLOR_BLUE = 'blue'
COLOR_PURPLE  = 'purple'
COLOR_YELLOW = 'yellow'
COLOR_PINK = 'pink'
COLOR_GREEN = 'green'
COLOR_ORANGE =  'orange'

COLOR_LIST = [
    (COLOR_RED, 'Rojo'),
    (COLOR_BLUE, 'Azul'),
    (COLOR_PURPLE, 'morado'),
    (COLOR_YELLOW, 'amarillo'),
    (COLOR_PINK, 'rosa'),
    (COLOR_GREEN, 'verde'),
    (COLOR_ORANGE, 'naranja')
]

STATUS_ACTIVE = 'active'
STATUS_INACTIVE = 'inactive'

STATUS_LIST = [
    (STATUS_ACTIVE, 'activo'),
    (STATUS_INACTIVE, 'inactivo')
]

HEADQUARTERS_OLAYA = 'Olaya'
HEADQUARTERS_AVENIDAMALL = 'Avenida Mall'
HEADQUARTERS_CENTROEMPRESARIAL = 'Centro Empresarial'


HEADQUARTERS_LIST = [
    (HEADQUARTERS_OLAYA, 'Olaya'),
    (HEADQUARTERS_AVENIDAMALL, 'Avenida Mall'),
    (HEADQUARTERS_CENTROEMPRESARIAL, 'Centro Empresarial'),
]


class Piloto(models.Model):
    name = models.CharField(max_length=50, null=False, unique=True, verbose_name='Nombre')
    status = models.CharField('status', choices=STATUS_LIST, max_length=30)
    color = models.CharField('Color', choices=COLOR_LIST, max_length=30)
    banner = models.CharField(max_length=70, null=False, verbose_name='texto banner')
    headquarters = models.CharField('Headquarters', choices=HEADQUARTERS_LIST, max_length=30)
    

    def __str__(self):
        return self.name
