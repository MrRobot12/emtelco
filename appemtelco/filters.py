'''
Filtros para el módulo de usuarios
UserFilter created by Andrés Felipe Blandón Palacio, Noviembre - 2018
'''
from django_filters import rest_framework as filters
from appemtelco.models.piloto import Piloto


class PilotoFilter(filters.FilterSet):
    '''
    Filtro para User y UserData
    '''
    class Meta:
        model = Piloto
        fields = {
            'id': ['in'],
            'headquarters': ['icontains', 'exact'],
            'name': ['icontains', 'exact'],
            'color': ['icontains', 'exact'],
            'banner': ['icontains', 'exact']
        }
