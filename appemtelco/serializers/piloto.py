from rest_framework import serializers
from appemtelco.models.piloto import *

class PilotoSerializer(serializers.ModelSerializer):

    class Meta():
        model = Piloto
        fields = ('id','name','status','color','banner','headquarters')

