# from django_filters import rest_framework as filters
from rest_framework.filters import OrderingFilter

from rest_framework import viewsets
from appemtelco.models.piloto import *
from appemtelco.serializers.piloto import *
from appemtelco.filters import PilotoFilter
#from appemtelco.filters.piloto import *


class PilotoView(viewsets.ModelViewSet):

    queryset = Piloto.objects.all()
    serializer_class = PilotoSerializer
    filter_class = PilotoFilter
    #search_fields = ('user', 'service', 'date', 'status')
    #filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
