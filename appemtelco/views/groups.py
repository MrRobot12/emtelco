from rest_framework import viewsets
from django.contrib.auth.models import Group
from appemtelco.serializers.groups import *

class GroupsViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupsSerializer