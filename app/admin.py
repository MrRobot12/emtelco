from django.contrib import admin
from app.models import User

# Se registra el modelo de usuarios que customizamos
admin.site.register(User)