'''
    UserSerializer by Juan David González Bedoya
    UserSerializer modified by Andrés Felipe Blandón, Mayo 2018
    UserSerializer modified by Andrés Felipe Blandón, Noviembre 2018
'''
import requests
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from django.contrib.auth.models import Group
from rest_framework import serializers

from app.models import LOGIN_APPLICATION, LOGIN_LDAP, LOGIN_TYPE_LIST, User
from src.validators import isnumbervalidator
from appemtelco.serializers.piloto import PilotoSerializer
from appemtelco.models.piloto import Piloto

class UserSerializer(serializers.ModelSerializer):
    '''
    User serializer
    '''

    email = serializers.EmailField(label='Correo electrónico', required=False, allow_null=True, allow_blank=True)
    first_name = serializers.CharField(label='Nombre(s)', required=False, allow_null=True, allow_blank=True)
    last_name = serializers.CharField(label='Apellido(s)', required=False, allow_null=True, allow_blank=True)
    password = serializers.CharField(label='Contraseña', required=False, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    confirm_password = serializers.CharField(label='Confirme contraseña', required=False, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    login_type = serializers.ChoiceField(label='Tipo de login', choices=LOGIN_TYPE_LIST, required=True, write_only=False)



    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'data' in kwargs:
            if 'partial' in kwargs:
                self.fields['first_name'].required = False
                self.fields['first_name'].allow_blank = True
                self.fields['last_name'].required = False
                self.fields['last_name'].allow_blank = True
                self.fields['email'].required = False
                self.fields['email'].allow_blank = True
                self.fields['password'].required = False
                self.fields['password'].allow_blank = True
                self.fields['confirm_password'].required = False
                self.fields['confirm_password'].allow_blank = True
            elif kwargs['data']['login_type'] == LOGIN_APPLICATION:
                self.fields['first_name'].required = True
                self.fields['first_name'].allow_null = False
                self.fields['first_name'].allow_blank = False
                self.fields['last_name'].required = True
                self.fields['last_name'].allow_null = False
                self.fields['last_name'].allow_blank = False
                self.fields['email'].required = True
                self.fields['email'].allow_null = False
                self.fields['email'].allow_blank = False
                self.fields['password'].required = True
                self.fields['password'].allow_null = False
                self.fields['password'].allow_blank = False
                self.fields['confirm_password'].required = True
                self.fields['confirm_password'].allow_null = False
                self.fields['confirm_password'].allow_blank = False
            
    def validate(self, data):
        if data['login_type'] == LOGIN_LDAP:
            try:
                ws_reponse = requests.post(
                    settings.PYPERSONAL_URL,
                    {
                        'user': data['username'],
                        'fields': '["first_name", "last_name", "mail"]'
                    }
                )

                content = ws_reponse.json()
                if ws_reponse.status_code == requests.codes['ok']:
                    data['first_name'] = content['epersonal']['first_name']
                    data['last_name'] = content['epersonal']['last_name']
                    data['email'] = content['ldap']['mail']
                elif ws_reponse.status_code == requests.codes['not_found']:
                    raise serializers.ValidationError(content['user'])
                elif ws_reponse.status_code == requests.codes['forbidden']:
                    raise serializers.ValidationError(content['general'])
                elif ws_reponse.status_code == requests.codes['internal_server_error']:
                    raise serializers.ValidationError(content['general'])
                else:
                    raise serializers.ValidationError('Ocurrió un error al recolectar datos del directorio activo')
            except Exception as e:
                raise serializers.ValidationError(e.detail)
            
            print(data)
        
        return data
    
    def update(self, instance, validated_data):

        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.is_superuser = validated_data.get('is_superuser', instance.is_superuser)
        instance.login_type = validated_data.get('login_type', instance.login_type)

        # instance.set_password(instance.password)
        instance.piloto = validated_data.get('piloto', instance.piloto)

        instance.groups.set(validated_data['groups'])
        
        instance.save()
        
        return instance
    
    def create(self, validated_data):
        groups_data = validated_data.pop('groups')
        try:
            validated_data.pop('confirm_password')
        except:
            pass

        user = User.objects.create_user(**validated_data)
        user.groups.set(groups_data)    
        return user
    
    def retrive(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def destroy(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response('detail:error', status=status.HTTP_204_NO_CONTENT) 
    
    piloto_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Piloto.objects.all(), source='piloto', label="Piloto", required=False, allow_null=True)
    
    piloto = PilotoSerializer(read_only=True)

    class Meta:
        '''
        Meta tags
        '''

        model = User
        fields = (
            'id',
            'username',
            'login_type',
            'email',
            'first_name',
            'last_name',
            'is_active',
            'is_superuser',
            'groups',
            'password',
            'confirm_password',
            'piloto_id',
            'piloto'
        )

class UpdatedPasswordSerializer(serializers.ModelSerializer):

    last_password = serializers.CharField(label='Contraseña actual', required=True, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    password = serializers.CharField(label='Contraseña nueva', required=True, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    confirm_password = serializers.CharField(label='Confirme contraseña', required=True, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)

    def update(self, instance, validated_data):

        # valida que la contraseña ingresadda sea igual a la que esta en BD
        validation = instance.check_password(validated_data.get('last_password'))

        try:
            if validation == True:

                if validated_data.get('password') == validated_data.get('confirm_password'):


                    instance.password = validated_data.get('password', instance.password)
                    instance.set_password(instance.password)
                    instance.save()
                                   
                    return instance

                else:
                    raise serializers.ValidationError({'password': 'Las credenciales proporsionadas no son validas.'})
            else:
                raise serializers.ValidationError({'last_password': 'No concuerda la contrasela actual - Las credenciales proporsionadas no son validas.'})
        except Exception as e:
            raise e

    # def update(self, instance, validated_data):

    #     instance.password = validated_data.get('password', instance.password)

    #     instance.set_password(instance.password)

    #     instance.save()

    #     return instance

    class Meta():
        model = User
        fields = (
            'id',
            'last_password',
            'password',
            'confirm_password'
        )


class PilotoAsignSerializer(serializers.ModelSerializer):

    def update(self, instance, validated_data):

        try:
            
            instance.piloto = validated_data.get('piloto', instance.piloto)
            instance.save()
            return instance

        except Exception as e:
            raise serializers.ValidationError({'detail': 'Error al guardar el piloto'})

    class Meta():
        model = User
        fields = (
            'id',
            'piloto'
        )

