'''
    Modelo para usuarios
    User and UserManager created by Andrés Felipe Blandón Palacio, Noviembre - 2018
'''

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from src.validators import isnumbervalidator
from appemtelco.models.piloto import Piloto

# Constantes para los tipos de loguin dentro de emtelco.
LOGIN_LDAP = 'LDAP'
LOGIN_APPLICATION = 'APPLICATION'

LOGIN_TYPE_LIST = [
    (LOGIN_LDAP, 'Directorio activo'),
    (LOGIN_APPLICATION, 'Aplicación')
]

class UserManager(BaseUserManager):

    def create_user(self, username, email, login_type, password=None, **extra_fields):
        """
        """

        user = self.model(
            username=username,
            email=self.normalize_email(email),
            login_type=login_type,
            **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, login_type, password):
        """
        """
        user = self.create_user(
            email=email,
            username=username,
            password=password,
            login_type=login_type,
        )
        user.is_superuser = True
        user.save(using=self._db)
        return user

class User(AbstractUser):
    '''
    Campos adicionales para el modelo de usuarios
    '''
    login_type = models.CharField('Tipo de login', choices=LOGIN_TYPE_LIST, max_length=20)
    piloto = models.ForeignKey(Piloto, on_delete=models.PROTECT, null=True, verbose_name='Piloto')

    objects = UserManager()

    REQUIRED_FIELDS = ['email', 'login_type']

    class Meta:
        '''
        Meta tags
        '''
        verbose_name_plural = 'Usuarios'
        verbose_name = 'Usuario'
        # Permisos por defecto desactivados
        default_permissions = ()
        # Se crear los propios permisos y de esta forma tenerlos en español
        permissions = (
            # Usuarios
            ('add_user', 'Crear usuarios'),
            ('change_user', 'Actualizar un usuario'),
            ('list_user', 'Consultar usuarios'),
            ('retrieve_user', 'Consultar un usuario'),
            # Grupos
            ('add_groups', 'Crear un grupo'),
            ('change_groups', 'Actualizar un grupo'),
            ('list_groups', 'Consultar grupos'),
            ('retrieve_groups', 'Consultar un grupo')
        )
       
