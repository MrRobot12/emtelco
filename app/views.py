'''
Vista de usuarios
UserViewSet created by Andrés Felipe Blandón Palacio, Noviembre - 2018
'''

from django_filters import rest_framework as filters
from rest_framework.filters import OrderingFilter

from src.view import BaseViewSet
from app.models import User
from app.serializer import UserSerializer, UpdatedPasswordSerializer, PilotoAsignSerializer
from app.filters import UserFilter 
class UserViewSet(BaseViewSet):
    '''
    ViewSet para usuarios
    '''

    app_code = 'app'
    permission_code = 'user'
    queryset = User.objects.all().prefetch_related('groups').select_related('piloto')
    serializer_class = UserSerializer
    filter_class = UserFilter
    search_fields = ('username', 'first_name', 'last_name', 'email', 'pilotO')
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

    def create(self, request):

        self.serializer_class = UserSerializer

        return super().create(request)

class UpdatedPasswordViewSet(BaseViewSet):
    '''
    ViewSet para actualizar contraseña
    '''

    app_code = 'app'
    permission_code = 'user'
    queryset = User.objects.all()
    serializer_class = UpdatedPasswordSerializer
    

class UserPViewSet(BaseViewSet):
  
    app_code = 'app'
    permission_code = 'user'
    queryset = User.objects.filter(piloto_id=None)
    serializer_class = UserSerializer
    filter_class = UserFilter

class PilotoAsignViewSet(BaseViewSet):
    app_code = 'app'
    permission_code = 'user'
    queryset = User.objects.all()
    serializer_class = PilotoAsignSerializer