"""emtelco URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from appemtelco.views.piloto import PilotoView
from appemtelco.views.groups import GroupsViewSet
from app.views import UserViewSet, UpdatedPasswordViewSet, UserPViewSet, PilotoAsignViewSet

router = DefaultRouter()

router.register('users', UserViewSet, base_name='users')
router.register('piloto', PilotoView, base_name='piloto')
router.register('updatedPassword', UpdatedPasswordViewSet, base_name='updatedPassword')
router.register('userP', UserPViewSet, base_name='userP')
router.register('groups', GroupsViewSet, base_name='Group')
router.register('pilotoAsign', PilotoAsignViewSet, base_name='pilotoAsign')
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),    
    path('api/login/', obtain_jwt_token),
    path('api-auth/', include('rest_framework.urls')) 
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns